<?xml version="1.0" encoding="UTF-8"?>
<Project Version="3" Minor="2" Path="C:/Users/321squarial/Desktop/cam">
    <Project_Created_Time></Project_Created_Time>
    <TD_Encoding>UTF-8</TD_Encoding>
    <TD_Version>5.6.59063</TD_Version>
    <UCode>11000111</UCode>
    <Name>cam</Name>
    <HardWare>
        <Family>EG4</Family>
        <Device>EG4S20BG256</Device>
        <Speed></Speed>
    </HardWare>
    <Source_Files>
        <Verilog>
            <File Path="sources/rtl/camera_init.v">
                <FileInfo>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="1"/>
                </FileInfo>
            </File>
            <File Path="sources/rtl/i2c_module.v">
                <FileInfo>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="2"/>
                </FileInfo>
            </File>
            <File Path="sources/rtl/lcd_sync.v">
                <FileInfo>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="3"/>
                </FileInfo>
            </File>
            <File Path="sources/rtl/test_camera.v">
                <FileInfo>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="4"/>
                </FileInfo>
            </File>
            <File Path="al_ip/ip_pll.v">
                <FileInfo>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="5"/>
                </FileInfo>
            </File>
            <File Path="sources/rtl/camera_reader.v">
                <FileInfo>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="6"/>
                </FileInfo>
            </File>
            <File Path="CNN_READ.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="7"/>
                </FileInfo>
            </File>
            <File Path="CNN_L1_221009.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="8"/>
                </FileInfo>
            </File>
            <File Path="test_ram_read.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="9"/>
                </FileInfo>
            </File>
            <File Path="POOL_CORE.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="12"/>
                </FileInfo>
            </File>
            <File Path="CNN3.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="13"/>
                </FileInfo>
            </File>
            <File Path="POOL3.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="14"/>
                </FileInfo>
            </File>
            <File Path="POOL_READ_P1.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="16"/>
                </FileInfo>
            </File>
            <File Path="CNN_READ_C2.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="17"/>
                </FileInfo>
            </File>
            <File Path="POOL_READ_P2.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="18"/>
                </FileInfo>
            </File>
            <File Path="RGB2GRAY.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="19"/>
                </FileInfo>
            </File>
            <File Path="PRE_LAYER.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="21"/>
                </FileInfo>
            </File>
            <File Path="READ_RAMP2.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="23"/>
                </FileInfo>
            </File>
            <File Path="conv1.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="25"/>
                </FileInfo>
            </File>
            <File Path="pool1_layer.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="26"/>
                </FileInfo>
            </File>
            <File Path="conv2.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="28"/>
                </FileInfo>
            </File>
            <File Path="full_layer.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="30"/>
                </FileInfo>
            </File>
            <File Path="D:/YJS_TCL_FPGA/22_11_1_py_test/CNN-Implementation-in-Verilog-master/CNN-Implementation-in-Verilog-master/cnn_verilog/comparator.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="32"/>
                </FileInfo>
            </File>
            <File Path="D:/YJS_TCL_FPGA/22_11_1_py_test/CNN-Implementation-in-Verilog-master/CNN-Implementation-in-Verilog-master/cnn_verilog/conv1_buf.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="33"/>
                </FileInfo>
            </File>
            <File Path="D:/YJS_TCL_FPGA/22_11_1_py_test/CNN-Implementation-in-Verilog-master/CNN-Implementation-in-Verilog-master/cnn_verilog/conv1_calc.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="34"/>
                </FileInfo>
            </File>
            <File Path="D:/YJS_TCL_FPGA/22_11_1_py_test/CNN-Implementation-in-Verilog-master/CNN-Implementation-in-Verilog-master/cnn_verilog/conv1_layer.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="35"/>
                </FileInfo>
            </File>
            <File Path="D:/YJS_TCL_FPGA/22_11_1_py_test/CNN-Implementation-in-Verilog-master/CNN-Implementation-in-Verilog-master/cnn_verilog/conv2_buf.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="36"/>
                </FileInfo>
            </File>
            <File Path="D:/YJS_TCL_FPGA/22_11_1_py_test/CNN-Implementation-in-Verilog-master/CNN-Implementation-in-Verilog-master/cnn_verilog/conv2_calc_1.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="37"/>
                </FileInfo>
            </File>
            <File Path="D:/YJS_TCL_FPGA/22_11_1_py_test/CNN-Implementation-in-Verilog-master/CNN-Implementation-in-Verilog-master/cnn_verilog/conv2_calc_2.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="38"/>
                </FileInfo>
            </File>
            <File Path="D:/YJS_TCL_FPGA/22_11_1_py_test/CNN-Implementation-in-Verilog-master/CNN-Implementation-in-Verilog-master/cnn_verilog/conv2_calc_3.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="39"/>
                </FileInfo>
            </File>
            <File Path="D:/YJS_TCL_FPGA/22_11_1_py_test/CNN-Implementation-in-Verilog-master/CNN-Implementation-in-Verilog-master/cnn_verilog/conv2_layer.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="40"/>
                </FileInfo>
            </File>
            <File Path="D:/YJS_TCL_FPGA/22_11_1_py_test/CNN-Implementation-in-Verilog-master/CNN-Implementation-in-Verilog-master/cnn_verilog/fully_connected.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="41"/>
                </FileInfo>
            </File>
            <File Path="D:/YJS_TCL_FPGA/22_11_1_py_test/CNN-Implementation-in-Verilog-master/CNN-Implementation-in-Verilog-master/cnn_verilog/maxpool_relu.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="42"/>
                </FileInfo>
            </File>
            <File Path="D:/YJS_TCL_FPGA/22_11_1_py_test/CNN-Implementation-in-Verilog-master/CNN-Implementation-in-Verilog-master/cnn_verilog/top_test_consume.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="43"/>
                </FileInfo>
            </File>
            <File Path="conv1_layer1.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="44"/>
                </FileInfo>
            </File>
            <File Path="conv2_layer2.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="45"/>
                </FileInfo>
            </File>
            <File Path="pool_core0.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="46"/>
                </FileInfo>
            </File>
            <File Path="output_layer.v">
                <FileInfo>
                    <Attr Name="AutoExcluded" Val="true"/>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="48"/>
                </FileInfo>
            </File>
        </Verilog>
        <ADC_FILE>
            <File Path="sources/sdc/SparkRoad.adc">
                <FileInfo>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="constraint_1"/>
                    <Attr Name="CompileOrder" Val="1"/>
                </FileInfo>
            </File>
        </ADC_FILE>
        <SDC_FILE>
            <File Path="D:/BaiduNetdiskDownload/SparkRoad-V-master/demo_v_5_6/22_10_11_test_cam/cam.sdc">
                <FileInfo>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="constraint_1"/>
                    <Attr Name="CompileOrder" Val="2"/>
                </FileInfo>
            </File>
        </SDC_FILE>
        <IP_FILE>
            <File Path="al_ip/ip_ram.ipc">
                <FileInfo>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="10"/>
                </FileInfo>
            </File>
            <File Path="al_ip/01.ipc">
                <FileInfo>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="11"/>
                </FileInfo>
            </File>
            <File Path="al_ip/02.ipc">
                <FileInfo>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="15"/>
                </FileInfo>
            </File>
            <File Path="al_ip/RAM03_512.ipc">
                <FileInfo>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="20"/>
                </FileInfo>
            </File>
            <File Path="al_ip/RAM04_1024.ipc">
                <FileInfo>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="22"/>
                </FileInfo>
            </File>
            <File Path="al_ip/RAM12_1024.ipc">
                <FileInfo>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="24"/>
                </FileInfo>
            </File>
            <File Path="al_ip/RAM12_512.ipc">
                <FileInfo>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="27"/>
                </FileInfo>
            </File>
            <File Path="al_ip/RAM14_256.ipc">
                <FileInfo>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="29"/>
                </FileInfo>
            </File>
            <File Path="al_ip/RAM12_128.ipc">
                <FileInfo>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="31"/>
                </FileInfo>
            </File>
            <File Path="al_ip/RAM20_191.ipc">
                <FileInfo>
                    <Attr Name="UsedInSyn" Val="true"/>
                    <Attr Name="UsedInP&R" Val="true"/>
                    <Attr Name="BelongTo" Val="design_1"/>
                    <Attr Name="CompileOrder" Val="47"/>
                </FileInfo>
            </File>
        </IP_FILE>
    </Source_Files>
    <FileSets>
        <FileSet Name="design_1" Type="DesignFiles">
        </FileSet>
        <FileSet Name="constraint_1" Type="ConstrainFiles">
        </FileSet>
    </FileSets>
    <TOP_MODULE>
        <LABEL></LABEL>
        <MODULE>test_camera</MODULE>
        <CREATEINDEX>user</CREATEINDEX>
    </TOP_MODULE>
    <Property>
    </Property>
    <Device_Settings>
    </Device_Settings>
    <Configurations>
    </Configurations>
    <Runs>
        <Run Name="syn_1" Type="Synthesis" ConstraintSet="constraint_1" Description="" Active="true">
            <Strategy Name="Default_Synthesis_Strategy">
                <GateProperty>
                    <opt_timing>medium</opt_timing>
                </GateProperty>
            </Strategy>
        </Run>
        <Run Name="phy_1" Type="PhysicalDesign" ConstraintSet="constraint_1" Description="" SynRun="syn_1" Active="true">
            <Strategy Name="Default_PhysicalDesign_Strategy">
                <PlaceProperty>
                    <opt_timing>high</opt_timing>
                </PlaceProperty>
                <RouteProperty>
                    <effort>high</effort>
                    <opt_timing>high</opt_timing>
                </RouteProperty>
            </Strategy>
        </Run>
    </Runs>
    <Project_Settings>
        <Step_Last_Change>2019-10-01 20:34:42</Step_Last_Change>
        <Current_Step>0</Current_Step>
        <Step_Status>true</Step_Status>
    </Project_Settings>
</Project>
