/**
 * Author: Karbon
 * GitHub: https://github.com/csy-tvgo
 * Date:   2022-May-9
 * Doc:    https://verimake.com/d/189
 */

`timescale 1ns/ 1ps
module Camera_usb_top (input wire clk_24m,
                        input wire rst_n,
                        input wire cam_pclk,
                        output wire cam_xclk,
                        input wire cam_href,
                        input wire cam_vsync,
                        output wire cam_pwdn,
                        output wire cam_rst,
                        output wire cam_soic,
                        inout wire cam_soid,
                        input wire [7: 0] cam_data,

                        output	 	 usb_pclk, 	
                        output		 usb_wr, 
                        output		 usb_rd, 	
                        output [7:0] usb_dq,
                        output       usb_pa7,	//usb_cs
                        output       usb_pa6,	//usb_pkt
                        output       usb_pa5,	//usb_addr1
                        output       usb_pa4,	//usb_addr0
                        output       usb_pa2,	//usb_oe
                        output       usb_pa1,	//usb_int1
                        output       usb_pa0,	//usb_int0
                            
                        input		 usb_flaga,     //reserve
                        input		 usb_flagb,		//full
                        input		 usb_flagc		//empty
                        );
    
    // 功能：读取摄像头画面，通过 usb 发送
    
    //时钟
    wire   clk_cam;
    wire   clk_sccb;
    wire   clk_48mhz;
    
    //摄像头连线
    wire        camera_wrreq;
    wire        camera_wclk;
    wire [15:0] camera_wrdat;
    wire [15:0] camera_addr;
    
    reg   init_state;
    wire   init_ready;
    wire   sda_oe;
    wire   sda;
    wire   sda_in;
    wire   scl;
    
    assign cam_soid = (sda_oe == 1'b1) ? sda : 1'bz;
    assign sda_in   = cam_soid;
    assign cam_soic = scl;
    assign cam_pwdn = 1'b0;
    assign cam_rst  = rst_n;
    
    // usb 图传连线
    wire cam_wlock;
    wire [15:0] img_rddat;
    wire [15:0] img_rdaddr;

    reg [19:0] rst_cnt; 
    reg rstn;
    
    // 例化各个模块
    
    ip_pll u_pll(
    .refclk(clk_24m),  // 24M
    .reset(!rst_n),
    .clk0_out(clk_cam),  // 12M, for cam xclk
    .clk1_out(clk_sccb), // 4M, for sccb init
    .clk2_out(clk_48mhz)
    );
    
    camera_init u_camera_init
    (
    .clk(clk_sccb),
    .reset_n(rst_n),
    .ready(init_ready),
    .sda_oe(sda_oe),
    .sda(sda),
    .sda_in(sda_in),
    .scl(scl)
    );
    
    camera_reader u_camera_reader
    (
    .clk  (clk_cam),
    .reset_n (rst_n),
    .csi_xclk (cam_xclk),
    .csi_pclk (cam_pclk),
    .csi_data (cam_data),
    .csi_vsync (!cam_vsync),
    .csi_hsync (cam_href),
    .data_out (camera_wrdat),
    .wrreq  (camera_wrreq),
    .wrclk  (camera_wclk),
    .wraddr  (camera_addr)
    );
    
    img_cache u_img_cache
    (
    //write 32800*16
    .dia  (camera_wrdat),
    .addra  (camera_addr),
    .cea  (camera_wrreq/* & (!cam_wlock)*/),
    .clka  (camera_wclk),
    //read 32800*16
    .dob   (img_rddat),
    .addrb (img_rdaddr),
    .ceb   (cam_wlock),
    .clkb  (clk_48mhz)
    );

    usb_send #(
        .width  		( 200 		),
        .height 		( 164 		),
        .resize 		( 1   		))
    u_usb_send(
        //ports
        .CLK_48MHZ  		( clk_48mhz  		),
        .rst_n      		( rstn      		),
        .img_rdaddr 		( img_rdaddr 		),
        .img_rddat  		( img_rddat  		),
        .cam_wlock  		( cam_wlock  		),
        .camera_addr		( camera_addr		),
        .usb_pclk   		( usb_pclk   		),
        .usb_wr     		( usb_wr     		),
        .usb_rd     		( usb_rd     		),
        .usb_dq     		( usb_dq     		),
        .usb_pa7    		( usb_pa7    		),
        .usb_pa6    		( usb_pa6    		),
        .usb_pa5    		( usb_pa5    		),
        .usb_pa4    		( usb_pa4    		),
        .usb_pa2    		( usb_pa2    		),
        .usb_pa1    		( usb_pa1    		),
        .usb_pa0    		( usb_pa0    		),
        .usb_flaga  		( usb_flaga  		),
        .usb_flagb  		( usb_flagb  		),
        .usb_flagc  		( usb_flagc  		)
    );

    //----------Soft reset----------
    always @(posedge clk_48mhz)
        if(!rst_n)
            rst_cnt <= 0;
        else if(rst_cnt>='hf_fffa)
            rst_cnt <= rst_cnt;
        else
            rst_cnt <= rst_cnt + 1'b1;
            
    always @(posedge clk_48mhz)
        if(!rst_n)
            rstn <= 0;
        else if(rst_cnt>='hf_fff0)
            rstn <= 1'b1;
        else
            rstn <= 0;	

    
endmodule
