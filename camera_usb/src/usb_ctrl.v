//file name:	usb_ctrl.v
//author:		ETree
//date:			2018.1.9
//function:		USB Slave FIFO controllor
//log:

module usb_ctrl(
	input rst_n,
	input ext_pclk,

	output 	[7:0]		usb_dq, 	
	output			 	usb_pclk, 
	output				usb_cs, 
	output		 		usb_wr, 
	output				usb_rd, 
	output				usb_oe, 
	output				usb_a0, 
	output				usb_a1, 
	output				usb_pkt,	
	output				usb_pa0,
	input               usb_flaga,
	input               usb_flagb,
	input               usb_flagc,

    input               usb_we,
    input   [7:0]       usb_data
);

reg ext_wr;

localparam ST_IN_IDLE  = 1'b0;
localparam ST_IN_WRITE = 1'b1;

reg mystate;

always @(posedge ext_pclk or negedge rst_n)
	if(~rst_n)
	begin
		mystate <= ST_IN_IDLE;
		ext_wr <= 1'b0;
	end
	else
	case(mystate)
	ST_IN_IDLE:
	begin
		if(usb_flagb)
			mystate <= ST_IN_WRITE;
		else
			mystate <= ST_IN_IDLE;
		ext_wr <= 1'b0;
	end
	ST_IN_WRITE:
	begin
		if(usb_flagb)
		begin
			mystate <= ST_IN_WRITE;
			ext_wr <= 1'b1;
		end
		else
		begin
			mystate <= ST_IN_IDLE;
			ext_wr <= 1'b0;
		end
	end
	default:;
	endcase
		

assign usb_pa7  = 1'b0;
assign usb_rd   = 1'b1;
assign usb_wr   = ~ext_wr;
assign usb_pa2  = 1'b1;

assign usb_pa4  = 1'b0;
assign usb_pa5  = 1'b0;

assign usb_pa6  = 1'b1;

assign usb_pclk = ~ext_pclk;
assign usb_dq   = usb_data;
assign usb_pa0  = 1'b1;

endmodule
