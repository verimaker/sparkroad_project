/**
 * Author: Karbon
 * GitHub: https://github.com/csy-tvgo
 * Date:   2022-May-9
 * Doc:    https://verimake.com/d/189
 */

module usb_send #(parameter width = 200,        // 图像宽度
                     parameter height = 164,       // 图像高度
                     parameter resize = 1)         // 缩小倍数（若缩小会传输更快，请设为宽度和高度的公约数）
                    (input CLK_48MHZ,
                     input rst_n,
                     output reg [15:0] img_rdaddr,
                     input [15:0] img_rddat,
                     input [15:0] camera_addr,
                     output reg cam_wlock,  // 这个寄存器若为 1，暂停让摄像头向 BRAM 里写入数据
                     //USB2.0
                    output	 	 usb_pclk, 	
                    output		 usb_wr, 
                    output		 usb_rd, 	
                    output [7:0] usb_dq,
                    output       usb_pa7,	//usb_cs
                    output       usb_pa6,	//usb_pkt
                    output       usb_pa5,	//usb_addr1
                    output       usb_pa4,	//usb_addr0
                    output       usb_pa2,	//usb_oe
                    output       usb_pa1,	//usb_int1
                    output       usb_pa0,	//usb_int0
                        
                    input		 usb_flaga,     //reserve
                    input		 usb_flagb,		//full
                    input		 usb_flagc		//empty
                    );
    
    // 功能：从 BRAM 里读图像并通过 usb 发送出去
    
    reg [15: 0] pixel_x = 'd0;   // 当前像素的 x 方向坐标
    reg [15: 0] pixel_y = 'd0;   // 当前像素的 y 方向坐标
    
    reg [15: 0] datapack;        // 打包好的一像素数据，共 2B
    reg [7: 0] send_buf;         // 暂存待发的 1B 数据
    reg data_flag;
    reg usb_wr_reg;
    reg first_flag;
    reg usb_to_pc;

    always @(posedge CLK_48MHZ or negedge rst_n) begin
        if(!rst_n)
            usb_to_pc   <= 'b0;
        else if((!usb_to_pc && !usb_flagb) || !first_flag)
            usb_to_pc   <= 'b1;
        else if(usb_to_pc && !usb_flagc)
            usb_to_pc   <= 'b0;
    end
    
    always @(posedge CLK_48MHZ or negedge rst_n) begin
        if(!rst_n)
            data_flag   <= 'b0;
        else if(first_flag)
            data_flag   <= ~data_flag;
    end

    always @(posedge CLK_48MHZ or negedge rst_n) begin
        if(!rst_n)
            first_flag   <= 'b0;
        else if(camera_addr > 'd500)
            first_flag   <= 'b1;
    end

    always @(posedge CLK_48MHZ or negedge rst_n) begin
        if(!rst_n)
            img_rdaddr  <= 'h0;
        else if(!data_flag && !usb_to_pc)
            img_rdaddr  <= pixel_x + pixel_y * width;
    end

    always @(posedge CLK_48MHZ or negedge rst_n) begin
        if(!rst_n) begin
            pixel_x <= 'h0;
            pixel_y <= 'h0;
        end   
        else if(data_flag && pixel_x >= width - 1'b1 && usb_flagb) begin
            if(pixel_y >= height - 1'b1) begin
                pixel_x <= 'h0;
                pixel_y <= 'h0;
            end
            else begin
                pixel_x <= 'h0;
                pixel_y <= pixel_y + resize;
            end
        end 
        else if(data_flag && !usb_to_pc) begin
            pixel_x <= pixel_x + resize;
            pixel_y <= pixel_y;
        end
    end

    always @(posedge CLK_48MHZ or negedge rst_n) begin
        if(!rst_n)
            datapack    <= 'h0;
        else if(data_flag) begin
            // 打包第一字节数据
            datapack[15]    <= 1'b1;                                               // 固定值1
            datapack[14]    <= ((pixel_y == 'd0) && (pixel_x == 'd0))? 1'b1: 1'b0; // 帧头标记
            datapack[13]    <= (pixel_x == 'd0)? 1'b1: 1'b0;                       // 行头标记
            datapack[12: 9] <= img_rddat[15: 12];                                  // R[3:0]
            datapack[8]     <= img_rddat[10];                                      // G[3]
            // 打包第二字节数据
            datapack[7]     <= 1'b0;                                               // 固定值0
            datapack[6: 4]  <= img_rddat[9: 7];                                    // G[2:0]
            datapack[3: 0]  <= img_rddat[4: 1];                                    // B[3:0]
        end
    end

    always @(posedge CLK_48MHZ or negedge rst_n) begin
        if(!rst_n)
            send_buf    <= 'h0;
        else if(data_flag)
            send_buf    <= datapack[7: 0];
        else 
            send_buf    <= datapack[15: 8];
    end

    always @(posedge CLK_48MHZ or negedge rst_n) begin
        if(!rst_n) begin
            usb_wr_reg  <= 'b0;
            cam_wlock   <= 'b0;
        end
        else if(!usb_flagb || usb_to_pc) begin
            usb_wr_reg  <= 'b0;
            cam_wlock   <= 'b0;
        end
        else begin
            usb_wr_reg  <= 'b1;
            cam_wlock   <= 'b1;
        end
    end

    assign usb_pa7  = 1'b0;
    assign usb_rd   = 1'b1;
    // assign usb_wr   = ~usb_wr_reg;
    assign usb_wr   = !usb_flagb ? 1'b1 : usb_to_pc ? 1'b1 : 1'b0;
    assign usb_pa2  = 1'b1;

    assign usb_pa4  = 1'b0;
    assign usb_pa5  = 1'b0;

    assign usb_pa6  = 1'b1;

    assign usb_pclk = ~CLK_48MHZ;
    assign usb_dq   = send_buf;
    assign usb_pa0  = 1'b1;

    
endmodule
