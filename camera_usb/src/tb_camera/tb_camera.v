`timescale  1ns / 1ps

module tb_camera;

// Camera_usb_top Parameters
parameter PERIOD  = 42;

reg   clk_24m                              = 0 ;
reg   fifo_clk                             = 0 ;
reg   rst_n                                = 0 ;
reg   usb_flaga                            = 0 ;

wire  cam_rst                              ;
wire  usb_pclk                             ;
wire  usb_wr                               ;
wire  usb_rd                               ;
wire  [7:0]  usb_dq                        ;
wire  usb_pa7                              ;
wire  usb_pa6                              ;
wire  usb_pa5                              ;
wire  usb_pa4                              ;
wire  usb_pa2                              ;
wire  usb_pa1                              ;
wire  usb_pa0                              ;

initial
begin
    forever #(PERIOD/2)  clk_24m=~clk_24m;
end

initial
begin
    forever #1  fifo_clk =~fifo_clk;
end

initial
begin
    #(PERIOD*2) rst_n  =  1;
end

//时钟
    wire   clk_cam;
    wire   clk_sccb;
    wire   clk_48mhz;
    
    //摄像头连线
    wire        camera_wrreq;
    wire        camera_wclk;
    wire [15:0] camera_wrdat;
    wire [15:0] camera_addr;
    
    reg   init_state;
    wire   init_ready;
    wire   sda_oe;
    wire   sda;
    wire   sda_in;
    wire   scl;
    
    // usb 图传连线
    wire cam_wlock;
    wire [15:0] img_rddat;
    wire [15:0] img_rdaddr;
    
    wire 	cmos_pclk;
wire 	cmos_vsync;
wire 	cmos_href;
wire [7:0]	cmos_data;
wire    cmos_xclk;

    
    glbl glbl();
    
    ip_pll u_pll(
    .refclk(clk_24m),  // 24M
    .reset(!rst_n),
    .clk0_out(clk_cam),  // 12M, for cam xclk
    .clk1_out(clk_sccb), // 4M, for sccb init
    .clk2_out(clk_48mhz)
    );

    Video_Image_Simulate_CMOS #(
        .CMOS_VSYNC_VALID 		( 1'b1    		),
        .IMG_HDISP        		( 11'd200 		),
        .IMG_VDISP        		( 11'd164 		))
    u_Video_Image_Simulate_CMOS(
        //ports
        .rst_n      		( rst_n      		),
        .cmos_xclk  		( cmos_xclk  		),
        .cmos_pclk  		( cmos_pclk  		),
        .cmos_vsync 		( cmos_vsync 		),
        .cmos_href  		( cmos_href  		),
        .cmos_data  		( cmos_data  		)
    );
    
    camera_reader u_camera_reader
    (
    .clk  (clk_cam),
    .reset_n (rst_n),
    .csi_xclk (cmos_xclk),
    .csi_pclk (cmos_pclk),
    .csi_data (cmos_data),
    .csi_vsync (!cmos_vsync),
    .csi_hsync (cmos_href),
    .data_out (camera_wrdat),
    .wrreq  (camera_wrreq),
    .wrclk  (camera_wclk),
    .wraddr  (camera_addr)
    );
    
    img_cache u_img_cache
    (
    //write 32800*16
    .dia  (camera_wrdat),
    .addra  (camera_addr),
    .cea  (camera_wrreq /*& (!cam_wlock)*/),
    .clka  (camera_wclk),
    //read 32800*16
    .dob   (img_rddat),
    .addrb (img_rdaddr),
    .ceb   (cam_wlock),
    .clkb  (clk_48mhz)
    );

    usb_send #(
        .width  		( 200 		),
        .height 		( 164 		),
        .resize 		( 1   		))
    u_usb_send(
        //ports
        .CLK_48MHZ  		( clk_48mhz  		),
        .rst_n      		( rst_n      		),
        .camera_addr        ( camera_addr       ),
        .img_rdaddr 		( img_rdaddr 		),
        .img_rddat  		( img_rddat  		),
        .cam_wlock  		( cam_wlock  		),
        .usb_pclk   		( usb_pclk   		),
        .usb_wr     		( usb_wr     		),
        .usb_rd     		( usb_rd     		),
        .usb_dq     		( usb_dq     		),
        .usb_pa7    		( usb_pa7    		),
        .usb_pa6    		( usb_pa6    		),
        .usb_pa5    		( usb_pa5    		),
        .usb_pa4    		( usb_pa4    		),
        .usb_pa2    		( usb_pa2    		),
        .usb_pa1    		( usb_pa1    		),
        .usb_pa0    		( usb_pa0    		),
        .usb_flaga  		( usb_flaga  		),
        .usb_flagb  		( ~usb_flagb  		),
        .usb_flagc  		( ~usb_flagc  		)
    );

    wire    [7:0]   fifo_out;
    wire            fifo_re;

    usb_fifo u_usb_fifo(
        //ports
        .rst        		( !rst_n        	),
        .di         		( usb_dq    		),
        .clkw       		( usb_pclk       	),
        .we         		( ~usb_wr         	),
        .clkr       		( fifo_clk       	),
        .re         		( fifo_re         	),
        .do         		( fifo_out         	),
        .empty_flag 		( usb_flagc 		),
        .full_flag  		( usb_flagb  		)
    );

    assign fifo_re = usb_flagb ? 1'b1 : usb_flagc ? 1'b0 : fifo_re;


endmodule