import usb
import cv2
import numpy as np
import time
from datetime import datetime


def bytes_extract(raw_bytes, left, right):
    # extract the bit(s) of bytes
    try:
        value = (((1 << (left + 1)) - 1) & raw_bytes) >> right
        return int(value)
    except Exception as e:
        print(e)


def depack(data):
    # depack the data of one pixel
    begin_frame = bytes_extract(data, 14, 14)
    begin_line = bytes_extract(data, 13, 13)
    pixel_R = bytes_extract(data, 12, 9)
    pixel_G = (bytes_extract(data, 8, 8) << 3) | bytes_extract(data, 6, 4)
    pixel_B = bytes_extract(data, 3, 0)
    return begin_frame, begin_line, pixel_R, pixel_G, pixel_B


# cy_usb.rx_loop()
if __name__ == "__main__":
    dev = usb.core.find(idVendor=0x04B4, idProduct=0x1004)
    height = 164
    width = 200
    row = 0
    col = 0
    temp = 0
    temp_0 = 0
    frame_start = 0
    if dev is None:
        raise ValueError('Device not found')
    else:
        dev.set_configuration()
    width = 200  # 图像宽度
    height = 164  # 图像高度
    canvas = np.zeros((height, width, 3), dtype='uint8')

    while True:
        try:
            a = dev.read(0x82, 512, 1000)
            for data in a:
                if bytes_extract(data, 7, 7):
                    temp_0 = data << 8
                else:
                    temp = temp_0 + data
                    begin_frame, begin_line, pixel_R, pixel_G, pixel_B = depack(
                        temp)
                    color_r = pixel_R << 4
                    color_g = pixel_G << 4
                    color_b = pixel_B << 4
                    color = [color_b, color_g, color_r]  # 合并三
                    color = np.array(color)

                    if begin_frame and frame_start == 0:
                        frame_start = 1
                        begin_frame = 0

                    if frame_start:
                        if begin_frame:
                            cv2.imshow('RGB565 Image', canvas)
                            if cv2.waitKey(1) == ord('q'):
                                cv2.destroyAllWindows()
                                break
                            row = 0
                            frame_start = 0

                        if begin_line:
                            row = row + 1
                            col = 0
                        col = col + 1
                        if col <= 200 and row <= 164:
                            canvas[row-1, col-1, :] = color

        except Exception as e:
            print("[{}] Met an error: {}".format(datetime.now(), str(e)))
            print("[{}] Retrying...\n".format(datetime.now()))
            time.sleep(1)
            col = 0
            row = 0
