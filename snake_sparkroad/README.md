# 贪吃蛇-SparkRoad

#### 介绍
通过SparkRoad开发板实现了贪吃蛇功能。用VGA接口输出画面，板载按键进行位置控制。具体效果可见[FPGA交作业系列｜纯verilog实现的贪吃蛇游戏，交互效果甚好](https://www.bilibili.com/video/BV1U84y167Xp/?spm_id_from=333.337.search-card.all.click&vd_source=bcd71e773da97b6543533aec277a2ca4)