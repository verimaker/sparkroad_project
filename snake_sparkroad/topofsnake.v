module topofsnake
(
    input clk,
	input rst, 
	input up,
	input down,
	input left,
    input right,
	output hsync,
	output vsync,
	output vga_clk,
	output [7:0]vga_r,
    output [7:0]vga_g,
    output [7:0]vga_b,
	output [7:0]seg_out, 
	output [3:0]seg_con
);

	wire uppress;
	wire downpress;
	wire leftpress;
    wire rightpress;
	wire [1:0]snake;
    wire [5:0]foodx;
    wire [4:0]foody;
	wire [9:0]posx;
	wire [9:0]posy;
	wire [5:0]headx;
	wire [5:0]heady;
	wire hitwall;
    wire hitbody;
	wire add;
	wire die;
	wire restart;
	wire [6:0]cubenum;
	wire[1:0]status;	
	wire rst_n;
	wire locked;
	wire clk_25m;
    wire score_flag;
	wire [15:0] point_1 ;
	wire [15:0] point ;
    wire	clk_temp;
    wire	lock_0;
    wire	lock_1;

	pll_0 u_pll_0(
  		.refclk(clk),
  		.reset(~rst),
  		.extlock(lock_0),
  		.clk0_out(clk_temp) 
	);
    
    pll_1 u_pll_1(
  		.refclk(clk_temp),
  		.reset(~rst),
  		.extlock(lock_1),
  		.clk0_out(clk_25m) 
	);

	assign rst_n = rst & lock_0 & lock_1;

    CtrlGame Part1 (
        .clk(clk_25m),
	    .rst(rst_n),
	    .hitwall(hitwall),
        .hitbody(hitbody),
	    .key1(leftpress),
	    .key2(rightpress),
	    .key3(uppress),
	    .key4(downpress),
	    .die(die),
        .restart(restart),
        .status(status),
		.score_flag(score_flag),
		.point(point) 
	);
	
    Eatting Part2 (
        .clk(clk_25m),
		.rst(rst_n),
		.foodx(foodx),
		.foody(foody),
		.headx(headx),
		.heady(heady),
		.add(add)	
	);
	
	Snake Part3 (
	    .clk(clk_25m),
		.rst(rst_n),
		.leftpress(leftpress),
		.rightpress(rightpress),
		.uppress(uppress),
		.downpress(downpress),
		.snake(snake),
		.posx(posx),
		.posy(posy),
		.headx(headx),
		.heady(heady),
		.add(add),
		.status(status),
		.cubenum(cubenum),
		.hitbody(hitbody),
		.hitwall(hitwall),
		.die(die)
	);

	vga Part4 (
		.clk(clk_25m),
		.rst(rst),
		.point_1(point_1),
        .score_flag(score_flag),
		.hsync(hsync),
		.vsync(vsync),
		.snake(snake),
		.vga_clk(vga_clk),
        .vga_out({vga_r,vga_g,vga_b}),
		.posx(posx),
		.posy(posy),
		.foodx(foodx),
		.foody(foody)
	);
	
	Key Part5 (
		.clk(clk_25m),
		.rst(rst_n),
		.left(left),
		.right(right),
		.up(up),
		.down(down),
		.leftpress(leftpress),
		.rightpress(rightpress),
		.uppress(uppress),
		.downpress(downpress)		
	);
    
    Display Part6 (
		.clk(clk_25m),
		.rst(rst_n),	
		.add(add),
		.status(status),
		.seg_out(seg_out),
		.seg_con(seg_con),
		.point_1(point_1),
		.point(point)	
	);
endmodule
